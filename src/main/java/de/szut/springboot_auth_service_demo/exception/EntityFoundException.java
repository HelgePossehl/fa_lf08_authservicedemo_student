package de.szut.springboot_auth_service_demo.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EntityFoundException extends RuntimeException {

    Logger logger = LoggerFactory.getLogger(EntityFoundException.class);

    public EntityFoundException(String msg) {
        super(msg);
        logger.info("EntityFoundException.EntityFoundException");
    }

}
