package de.szut.springboot_auth_service_demo.service;

import de.szut.springboot_auth_service_demo.exception.EntityFoundException;
import de.szut.springboot_auth_service_demo.exception.EntityNotFoundException;
import de.szut.springboot_auth_service_demo.model.Account;
import de.szut.springboot_auth_service_demo.repository.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccountService {

    @Autowired
    private AccountRepository repository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    Logger logger = LoggerFactory.getLogger(AccountService.class);

    /**
     * Registriert einen neuen Account.
     * @param account Der zu registrierende Account.
     * @return Der registrierte Account.
     */
    public Account register(Account account) {
        logger.info("AccountService.register()");
        // Überprüfen, ob der Username schon registriert ist.
        Optional<Account> oAccount = repository.findById(account.getUsername());
        if (oAccount.isPresent()) {
            throw new EntityFoundException("Username '" + account.getUsername() + "' already assigned.");
        }
        // Password verschlüsseln und Account speichern.
        account.setPasswordEncrypted(passwordEncoder.encode(account.getPassword()));
        return repository.save(account);
    }

    /**
     * TODO: Nur zur Demo.
     * Ermittelt einen Account bezüglich eines Usernamens
     * @param username Der Username.
     * @return Der Account.
     */
    public Account getAccount(String username) {
        logger.info("AccountService.getAccount()");
        Optional<Account> oAccount = repository.findById(username);
        if (oAccount.isEmpty()) {
            throw new EntityNotFoundException("No account with username '" + username + "' found.");
        }
        return oAccount.get();
    }

    /**
     * TODO: Nur zur Demo.
     * Erstellt eine Liste aller Accounts.
     * @return Die Account-Info.
     */
    public List<Account> getAllAccounts() {
        logger.info("AccountService.getAllAccounts()");
        List<Account> accountList = repository.findAll();
        if (accountList.isEmpty()) {
            throw new EntityNotFoundException("No accounts found.");
        }
        return accountList;
    }

}
