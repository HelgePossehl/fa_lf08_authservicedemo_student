package de.szut.springboot_auth_service_demo.service;

import de.szut.springboot_auth_service_demo.exception.EntityNotFoundException;
import de.szut.springboot_auth_service_demo.model.JwtToken;
import de.szut.springboot_auth_service_demo.repository.AccountRepository;
import de.szut.springboot_auth_service_demo.model.Account;
import de.szut.springboot_auth_service_demo.security.JwtTokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthenticationService {

    Logger logger = LoggerFactory.getLogger(AuthenticationService.class);

    @Autowired
    private AccountRepository repository;
    @Autowired
    private JwtTokenService jwtTokenService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Generiert einen JWT-Token für einen gültigen Account.
     * @param account Der Account.
     * @return Der JWT-Token.
     */
    public JwtToken login(Account account) {
        Optional<Account> oAccount = repository.findById(account.getUsername());
        if (oAccount.isEmpty()) {
            throw new EntityNotFoundException("Username or password not valid.");
        }
        if (passwordEncoder.matches(account.getPassword(), oAccount.get().getPasswordEncrypted())) {
            return jwtTokenService.generateToken(account.getUsername());
        }
        else {
            throw new EntityNotFoundException("Username or password not valid.");
        }
    }

}
