package de.szut.springboot_auth_service_demo.response;

import lombok.Data;

@Data
public class AccountResponse {

    private String username;
    private String password;

}
