package de.szut.springboot_auth_service_demo.security;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {

    Logger logger = LoggerFactory.getLogger(JwtAuthenticationProvider.class);

    @Autowired
    JwtTokenService jwtTokenService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        logger.info("JwtAuthenticationProvider.authenticate()");
        try {
            String token = (String) authentication.getCredentials();
            String username = jwtTokenService.extractUsername(token);
            return new JwtAuthenticatedProfile(username);
        } catch (ExpiredJwtException e) {
            logger.error("JwtAuthenticationProvider.authenticate(): ExpiredJwtException");
            throw new JwtAuthenticationException("Token expired.");
        } catch (MalformedJwtException e) {
            logger.error("JwtAuthenticationProvider.authenticate(): MalformedJwtException");
            throw new JwtAuthenticationException("Token malformed.");
        } catch (SignatureException e) {
            logger.error("JwtAuthenticationProvider.authenticate(): SignatureException");
            throw new JwtAuthenticationException("Token signature invalid.");
        } catch (JwtException e) {
            logger.error("JwtAuthenticationProvider.authenticate(): Failed to verify token: " + e.getClass());
            throw new JwtAuthenticationException("Failed to verify token.");
        }
    }

    // UNNÖTG. War im Tutorial enthalten.
    //@Override
    public Authentication authenticateTutorial(Authentication authentication) throws AuthenticationException {
        logger.info("JwtAuthenticationProvider.authenticate()");
        try {
            String token = (String) authentication.getCredentials();
            String username = jwtTokenService.extractUsername(token);
            if (jwtTokenService.checkTokenNotExpired(token)) {
                return new JwtAuthenticatedProfile(username);
            }
            else {
                logger.error("JwtAuthenticationProvider.authenticate(): Token expired.");
                throw new JwtAuthenticationException("Token expired.");
            }
        } catch (JwtException ex) {
            logger.error("JwtAuthenticationProvider.authenticate(): Failed to verify token: " + ex.getMessage(), ex.getMessage());
            throw new JwtAuthenticationException("Failed to verify token.");
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        logger.info("JwtAuthenticationProvider.supports()");
        return JwtAuthenticatedToken.class.equals(aClass);
    }
}
