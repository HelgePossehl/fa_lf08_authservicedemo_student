package de.szut.springboot_auth_service_demo.response;

import lombok.Data;

@Data
public class JwtTokenResponse {

    private String token;

}
