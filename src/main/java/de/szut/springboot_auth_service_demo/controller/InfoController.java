package de.szut.springboot_auth_service_demo.controller;

import de.szut.springboot_auth_service_demo.mapper.InfoMapper;
import de.szut.springboot_auth_service_demo.model.Info;
import de.szut.springboot_auth_service_demo.response.InfoResponse;
import de.szut.springboot_auth_service_demo.service.InfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/SpringBootAuthService")
public class InfoController {

    Logger logger = LoggerFactory.getLogger(InfoController.class);

    @Autowired
    private InfoService service;
    @Autowired
    private InfoMapper mapper;

    @GetMapping("/info")
    public ResponseEntity<InfoResponse> getInfo() {
        logger.info("InfoController.getInfo()");
        Info info = service.createInfo();
        return new ResponseEntity<>(mapper.mapModelToResponse(info), HttpStatus.OK);
    }
}
