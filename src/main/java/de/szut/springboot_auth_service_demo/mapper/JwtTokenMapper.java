package de.szut.springboot_auth_service_demo.mapper;

import de.szut.springboot_auth_service_demo.model.JwtToken;
import de.szut.springboot_auth_service_demo.response.JwtTokenResponse;
import org.springframework.stereotype.Service;

@Service
public class JwtTokenMapper {

    /**
     * Mapped ein Model-Objekt in ein Response-Objekt.
     * @param model Das Model-Objekt.
     * @return  Das Response-Objekt.
     */
    public JwtTokenResponse mapModelToResponse(JwtToken model) {
        JwtTokenResponse response = new JwtTokenResponse();
        response.setToken(model.getToken());
        return response;
    }

}
