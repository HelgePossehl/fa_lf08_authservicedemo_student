package de.szut.springboot_auth_service_demo.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;

public class JwtAuthenticatedProfile implements Authentication {

    Logger logger = LoggerFactory.getLogger(JwtAuthenticatedProfile.class);

    private String username;

    public JwtAuthenticatedProfile(String username) {
        logger.info("JwtAuthenticatedProfile.JwtAuthenticatedProfile()");
        this.username = username;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return new ArrayList<>();
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        logger.info("JwtAuthenticatedProfile.getPrincipal()");
        return username;
    }

    @Override
    public boolean isAuthenticated() {
        return false;
    }

    @Override
    public void setAuthenticated(boolean b) throws IllegalArgumentException {

    }

    @Override
    public String getName() {
        return null;
    }
}
