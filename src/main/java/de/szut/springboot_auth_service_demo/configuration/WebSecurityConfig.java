package de.szut.springboot_auth_service_demo.configuration;

import de.szut.springboot_auth_service_demo.security.JwtAuthenticationEntryPoint;
import de.szut.springboot_auth_service_demo.security.JwtAuthenticationProvider;
import de.szut.springboot_auth_service_demo.security.JwtAuthenticationTokenFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@SuppressWarnings("SpringJavaAutowiringInspection")
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    Logger logger = LoggerFactory.getLogger(WebSecurityConfigurerAdapter.class);

    @Autowired
    private JwtAuthenticationProvider authenticationProvider;
    @Autowired
    private JwtAuthenticationEntryPoint authenticationEntryPoint;

    @Autowired
    public void configureAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder) {
        authenticationManagerBuilder.authenticationProvider(authenticationProvider);
    }

    @Bean
    public JwtAuthenticationTokenFilter getAuthenticationTokenFilter() {
        return new JwtAuthenticationTokenFilter();
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        logger.info("WebSecurityConfigurerAdapter.configure()");
        httpSecurity
                // Deaktivierung des Cross-Site Request Forgery (CSRF) Schutzes
                .csrf().disable()
                // Festlegung des AuthenticationEntryPoints (Fehlermeldung bei nicht authentifizierten Zugriff).
                .exceptionHandling().authenticationEntryPoint(authenticationEntryPoint).and()
                // Ausschalten der Nutzung bzw. Erstellung von Sessions.
                // Hat zur Folge, dass sich jeder Zugriff immer wieder neu autorisieren muss.
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                // Definition der Schnittstellen, die mit bzw. ohne Autorisierung konsumiert werden.
                .authorizeRequests()
                    // Definition der Schnittstellen, die ohne Autorisierung aufgerufen werden können.
                    .antMatchers("/SpringBootAuthService/noSecurity/*").permitAll()
                    .antMatchers("/SpringBootAuthService/register").permitAll()
                    .antMatchers("/SpringBootAuthService/login").permitAll()
                    // .antMatchers("/h2-console/*").permitAll()
                    // Alle anderen Schnittstellen können nur mit Autorisierung aufgerufen werden.
                    .anyRequest().authenticated().and()
                // Zuweisen des JWT-Filters zur FilterChain.
                .addFilterBefore(getAuthenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class)
                // Schaltet die Cache-Unterstützung ein, d.h. Caching wird unterbunden.
                .headers().cacheControl();
    }
}