package de.szut.springboot_auth_service_demo.request;

import lombok.Getter;

@Getter
public class AccountRequest {

    private String username;
    private String password;

}
