package de.szut.springboot_auth_service_demo.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EntityNotFoundException extends RuntimeException {

    Logger logger = LoggerFactory.getLogger(EntityNotFoundException.class);

    public EntityNotFoundException(String msg) {
        super(msg);
        logger.info("EntityNotFoundException.EntityNotFoundException");
    }

}
