package de.szut.springboot_auth_service_demo.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;

public class JwtAuthenticationException extends AuthenticationException {

    Logger logger = LoggerFactory.getLogger(JwtAuthenticationException.class);

    public JwtAuthenticationException(String msg) {
        super(msg);
        logger.info("JwtAuthenticationException.JwtAuthenticationException");
    }
}
