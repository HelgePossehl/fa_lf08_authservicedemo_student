package de.szut.springboot_auth_service_demo.security;

import de.szut.springboot_auth_service_demo.model.JwtToken;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;

@Service
public class JwtTokenService {

    Logger logger = LoggerFactory.getLogger(JwtTokenService.class);

    @Value("${jwt.issuer}")
    private String issuer;
    @Value("${jwt.secret}")
    private String secret;
    @Value("${jwt.expiration}")
    private Long expiration;

    /**
     * Generiert einen Token für einen Usernamen.
     * @param username Der Username.
     * @return Der Token.
     */
    public JwtToken generateToken(String username) {
        Date createdDate = new Date();
        Date expirationDate = calculateExpirationDate(createdDate);
        String token = Jwts.builder()
                .setClaims(new HashMap<>())
                .setIssuer(issuer)
                .setSubject(username)
                .setIssuedAt(createdDate)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
        JwtToken jwtToken = new JwtToken();
        jwtToken.setToken(token);
        return jwtToken;
    }

    /**
     * Berechnet die Ablaufzeit des Tokens.
     * @param createdDate Die Startzeit.
     * @return Die Ablaufzeit.
     */
    private Date calculateExpirationDate(Date createdDate) {
        logger.info("JwtTokenService.calculateExpirationDate()");
        return new Date(createdDate.getTime() + expiration * 1000);
    }

    /**
     * Extrahiert den Usernamen aus einem Token.
     * @param token Der zu untersuchende Token.
     * @return Der Username.
     */
    public String extractUsername(String token) {
        logger.info("JwtTokenService.extractUsername()");
        return extractAllClaims(token).getSubject();
    }

    /**
     * Spaltet den Token in seine Claims bzw. Key-Value-Paare auf.
     * @param token Der Token.
     * @return Die Hashmap aller Claims des Tokens.
     */
    private Claims extractAllClaims(String token) {
        logger.info("JwtTokenService.extractAllClaims()");
        return Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();
    }

    /**
     * UNNÖTG. War im Tutorial enthalten.
     * Überprüft, ob ein Token noch gültig bzw. noch nicht abgelaufen ist.
     * @param token Der zu prüfende Token.
     * @return  true:   Token ist noch gültig.
     *          false:  Token ist bereits abgelaufen.
     */
    public boolean checkTokenNotExpired(String token) {
        logger.info("JwtTokenService.isTokenNotExpired()");
        Date expiration = extractExpirationDate(token);
        return expiration.after(new Date());
    }

    /**
     * UNNÖTG. War im Tutorial enthalten.
     * Extrahiert die Ablaufzeit aus einem Token.
     * @param token Der zu untersuchende Token.
     * @return Die Ablaufzeit.
     */
    private Date extractExpirationDate(String token) {
        logger.info("JwtTokenService.extractExpirationDate()");
        return extractAllClaims(token).getExpiration();
    }
}
