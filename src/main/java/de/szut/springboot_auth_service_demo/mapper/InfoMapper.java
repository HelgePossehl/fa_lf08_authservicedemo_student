package de.szut.springboot_auth_service_demo.mapper;

import de.szut.springboot_auth_service_demo.model.Info;
import de.szut.springboot_auth_service_demo.response.InfoResponse;
import org.springframework.stereotype.Service;

@Service
public class InfoMapper {

    /**
     * Mapped ein Model-Objekt in ein Response-Objekt.
     * @param model Das Model-Objekt.
     * @return  Das Response-Objekt.
     */
    public InfoResponse mapModelToResponse(Info model) {
        InfoResponse response = new InfoResponse();
        response.setMessage(model.getMessage());
        return response;
    }

}
