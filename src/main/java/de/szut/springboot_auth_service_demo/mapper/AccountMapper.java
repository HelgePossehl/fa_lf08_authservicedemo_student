package de.szut.springboot_auth_service_demo.mapper;

import de.szut.springboot_auth_service_demo.model.Account;
import de.szut.springboot_auth_service_demo.request.AccountRequest;
import de.szut.springboot_auth_service_demo.response.AccountCompleteResponse;
import de.szut.springboot_auth_service_demo.response.AccountResponse;
import org.springframework.stereotype.Service;

@Service
public class AccountMapper {

    /**
     * Mapped ein Request-Objekt in ein Model-Objekt.
     * @param request   Das Request-Objekt.
     * @return  Das Model-Objekt.
     */
    public Account mapRequestToModel(AccountRequest request) {
        Account model = new Account();
        model.setUsername(request.getUsername());
        model.setPassword(request.getPassword());
        return model;
    }

    /**
     * Mapped ein Model-Objekt in ein Response-Objekt.
     * @param model Das Model-Objekt.
     * @return  Das Response-Objekt.
     */
    public AccountResponse mapModelToResponse(Account model) {
        AccountResponse response = new AccountResponse();
        response.setUsername(model.getUsername());
        response.setPassword(model.getPassword());
        return response;
    }

    /**
     * Mapped ein Model-Objekt in ein Response-Objekt.
     * @param model Das Model-Objekt.
     * @return  Das Response-Objekt.
     */
    public AccountCompleteResponse mapModelToResponseComplete(Account model) {
        AccountCompleteResponse response = new AccountCompleteResponse();
        response.setUsername(model.getUsername());
        response.setPassword(model.getPassword());
        response.setPasswordEncrypted(model.getPasswordEncrypted());
        return response;
    }
}
