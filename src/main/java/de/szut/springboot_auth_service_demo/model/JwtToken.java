package de.szut.springboot_auth_service_demo.model;

import lombok.Data;

@Data
public class JwtToken {

    private String token;

}
