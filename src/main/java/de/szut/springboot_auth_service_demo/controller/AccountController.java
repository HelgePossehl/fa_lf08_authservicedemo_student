package de.szut.springboot_auth_service_demo.controller;

import de.szut.springboot_auth_service_demo.mapper.AccountMapper;
import de.szut.springboot_auth_service_demo.model.Account;
import de.szut.springboot_auth_service_demo.request.AccountRequest;
import de.szut.springboot_auth_service_demo.response.AccountCompleteResponse;
import de.szut.springboot_auth_service_demo.response.AccountResponse;
import de.szut.springboot_auth_service_demo.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/SpringBootAuthService")
public class AccountController {

    Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private AccountService service;
    @Autowired
    private AccountMapper mapper;

    @PostMapping("/register")
    public ResponseEntity<AccountResponse> register(@RequestBody AccountRequest request) {
        logger.info("AccountController.register()");
        Account accountRequest = mapper.mapRequestToModel(request);
        Account accountResponse = service.register(accountRequest);
        return new ResponseEntity(mapper.mapModelToResponse(accountResponse), HttpStatus.OK);
    }

    // TODO: Nur zur Demo.
    @GetMapping("/noSecurity/accounts")
    public ResponseEntity<List<AccountResponse>> getAllAccounts() {
        logger.info("AccountController.getAllAccounts()");
        List<Account> accountList = service.getAllAccounts();
        List<AccountResponse> responseList = new ArrayList<>();
        for (Account account : accountList) {
            responseList.add(mapper.mapModelToResponse(account));
        }
        return new ResponseEntity<>(responseList, HttpStatus.OK);
    }

    // TODO: Nur zur Demo.
    @GetMapping("/noSecurity/accountsComplete")
    public ResponseEntity<List<AccountCompleteResponse>> getAllAccountsComplete() {
        logger.info("AccountController.getAllAccountsComplete()");
        List<Account> accountList = service.getAllAccounts();
        List<AccountCompleteResponse> responseList = new ArrayList<>();
        for (Account account : accountList) {
            responseList.add(mapper.mapModelToResponseComplete(account));
        }
        return new ResponseEntity<>(responseList, HttpStatus.OK);
    }

}
