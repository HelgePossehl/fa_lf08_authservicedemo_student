package de.szut.springboot_auth_service_demo.response;

import lombok.Data;

@Data
public class AccountCompleteResponse {

    private String username;
    private String password;
    private String passwordEncrypted;

}
