package de.szut.springboot_auth_service_demo.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Account {

    @Id
    private String username;
    private String password;
    private String passwordEncrypted;

}
