package de.szut.springboot_auth_service_demo.repository;

import de.szut.springboot_auth_service_demo.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

// Zugriff auf die Datenbank über http://localhost:8080/h2-console.
// JDBC-URL anpassen gemäß application.properties
public interface AccountRepository extends JpaRepository<Account, String> {
}
