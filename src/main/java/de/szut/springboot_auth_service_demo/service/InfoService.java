package de.szut.springboot_auth_service_demo.service;

import de.szut.springboot_auth_service_demo.model.Info;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class InfoService {

    Logger logger = LoggerFactory.getLogger(InfoService.class);

    /**
     * Erstellt einen Infotext.
     * @return Die Info.
     */
    public Info createInfo() {
        logger.info("InfoService.createInfo()");
        return new Info("Moin, moin!");
    }
}
