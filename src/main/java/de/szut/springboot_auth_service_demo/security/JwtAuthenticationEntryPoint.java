package de.szut.springboot_auth_service_demo.security;

import de.szut.springboot_auth_service_demo.controller.InfoController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

    Logger logger = LoggerFactory.getLogger(JwtAuthenticationEntryPoint.class);

    /**
     * Liefert bei Anfragen, die unautorisiert sind, den HTTP Status Code SC_UNAUTHORIZED zurück.
     * @param httpServletRequest    Das HTTP-Request.
     * @param httpServletResponse   Die HTTP-Response.
     * @param e                     Die Exception bei fehlerhafter Authentifizierung.
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        logger.info("JwtAuthenticationEntryPoint.commence()");
        httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }
}
