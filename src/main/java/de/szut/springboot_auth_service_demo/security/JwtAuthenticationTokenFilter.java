package de.szut.springboot_auth_service_demo.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    Logger logger = LoggerFactory.getLogger(JwtAuthenticationTokenFilter.class);

    private final String tokenHeader = "Authorization";
    private final String bearer = "Bearer ";

    /**
     * Überprüfen des eingehenden Tokens durch Spring Security.
     * @param httpServletRequest    Das HTTP-Request.
     * @param httpServletResponse   Die HTTP-Response.
     * @param filterChain           Die FilterChain.
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        logger.info("JwtAuthenticationTokenFilter.doFilterInternal(): " + httpServletRequest.getRequestURL());
        String token = filterToken(httpServletRequest);
        if (token != null) {
            logger.info("JwtAuthenticationTokenFilter.doFilterInternal(): token != null");
            JwtAuthenticatedToken authentication = new JwtAuthenticatedToken(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
        else {
            logger.info("JwtAuthenticationTokenFilter.doFilterInternal(): token == null");
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    /**
     * Extrahiert einen Token aus einem HTTP-Request.
     * @param request Das HTTP-Request.
     * @return Den Token.
     */
    private String filterToken(HttpServletRequest request) {
        logger.info("JwtAuthenticationTokenFilter.filterToken()");
        String requestHeader = request.getHeader(tokenHeader);
        if (requestHeader != null && requestHeader.startsWith(bearer)) {
            String token = requestHeader.substring(bearer.length());
            logger.info("JwtAuthenticationTokenFilter.filterToken(): token = " + token);
            return token;
        } else {
            logger.info("JwtAuthenticationTokenFilter.filterToken(): token = null");
            return null;
        }
    }

}
