package de.szut.springboot_auth_service_demo.controller;

import de.szut.springboot_auth_service_demo.mapper.AccountMapper;
import de.szut.springboot_auth_service_demo.mapper.JwtTokenMapper;
import de.szut.springboot_auth_service_demo.model.Account;
import de.szut.springboot_auth_service_demo.model.JwtToken;
import de.szut.springboot_auth_service_demo.request.AccountRequest;
import de.szut.springboot_auth_service_demo.response.JwtTokenResponse;
import de.szut.springboot_auth_service_demo.service.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/SpringBootAuthService")
public class AuthenticationController {

    Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

    @Autowired
    private AuthenticationService service;
    @Autowired
    private AccountMapper mapperAccount;
    @Autowired
    private JwtTokenMapper mapperJwtToken;

    @PostMapping("/login")
    public ResponseEntity<JwtTokenResponse> login(@RequestBody AccountRequest request) {
        logger.info("AuthenticationController.login()");
        Account accountRequest = mapperAccount.mapRequestToModel(request);
        JwtToken token = service.login(accountRequest);
        return new ResponseEntity(mapperJwtToken.mapModelToResponse(token), HttpStatus.OK);
    }

}
