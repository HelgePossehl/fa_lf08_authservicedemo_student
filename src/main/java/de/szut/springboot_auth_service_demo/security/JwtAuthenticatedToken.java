package de.szut.springboot_auth_service_demo.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class JwtAuthenticatedToken implements Authentication {

    Logger logger = LoggerFactory.getLogger(JwtAuthenticatedToken.class);

    private String token;

    public JwtAuthenticatedToken(String token) {
        logger.info("JwtAuthenticatedToken.JwtAuthenticatedToken()");
        this.token = token;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public Object getCredentials() {
        logger.info("JwtAuthenticatedToken.getCredentials()");
        return token;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }

    @Override
    public boolean isAuthenticated() {
        return false;
    }

    @Override
    public void setAuthenticated(boolean b) throws IllegalArgumentException { }

    @Override
    public String getName() {
        return null;
    }
}
